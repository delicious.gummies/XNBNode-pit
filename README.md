## XNBNode

This is a fork of [XNBNode](https://github.com/draivin/XNBNode) written by draivin. It is a command line utility to extract and pack XNB files.

Support is added only for Sword of the Stars: The Pit sprite configuration files (i.e. the files named *game_sprites.xnb*). It also supports XNBs that contain Pit sprites (the files referenced by the sprite configuration files).

If you want to modify the other Pit XNBs, then use **SotSTP\_XnbDescToText.exe** & **SotSTP\_XnbTextToDesc.exe** which can be downloaded at the [Japanese Pit Wiki](https://wikiwiki.jp/sotsthepit/gamemod)



## Compression
XNB files use a compression algorithm that is not easily available, this project uses a DLL that is probably proprietary for compression/decompression, and as such it was not included in the repository.
You can find the DLL [here](https://github.com/cpich3g/rpftool/blob/master/RPFTool/xcompress32.dll?raw=true).


## Usage
**You MUST download the above DLL and place it into the same folder as node.exe before running the below commands in command prompt!**

The following commands need to be run in `Command Prompt`

Ensure that your current directory in `Command Prompt` is set to the same folder you placed the DLL into. Example:

```
cd C:\Folder\with\the\above\dll
```

### XNB &rarr; YAML
`game_sprites.xnb` is the location of the XNB file you want to decode. This example assumes you placed it in the same folder as the DLL above.

`game_sprites.yaml` is where you want the decoded XNB file to be generated. This example assumes you want it in the same folder as the DLL above.
```
.\node.exe main.js extract game_sprites.xnb game_sprites.yaml
```
* The YAML file uses *nix line endings, you should use a text editor like [Notepad++](https://notepad-plus-plus.org/) or [Visual Studio Code](https://code.visualstudio.com/) to open and modify it. **Do not use notepad**!
* When modifying the YAML file ensure that you do not modify the **xnbData** object at the top of the file.
* Sprite entries can be added, removed, or modified by changing the entries under `content:  #!PitSpriteLibraryReader`

### YAML &rarr; XNB
`game_sprites.yaml` is the location of the YAML file that you modified and want to convert to an XNB. This example assumes you placed it in the same folder as the DLL above.

`game_sprites_new.xnb` is where you want the new XNB file to be generated. This example assumes you want it in the same folder as the DLL above.

```
.\node.exe main.js pack game_sprites.yaml game_sprites_new.xnb
```

